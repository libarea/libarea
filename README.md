# libarea

A platform for collective blogs and social media platform, forum, question and answer service. Catalog of sites (programs), site navigation and directories - facets. A community based on the PHP HLEB micro-framework.

## For testing

* Set up the web server configuration: public/ (The Public Directory)
* dev.sql
* settings: config/dbase.config.php and other files in the directory
* Log in to your account using administrator credentials: `ss@sdf.ru` / `qwer14qwer14`
* Or user: `test@test.ru` / `test@test.ru`

PHP 7.4+, MySQL 8+ or > MariaDB 10.2.2

### Demo

https://libarea.ru/

MIT License